<!DOCTYPE html>
<html lang="hu-HU">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="addPlanets.css" />
    <!-- <script type="text/javascript" src=""></script> -->
    <!-- Authentication -->
    <?php
    session_start();
    if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] == false){
        header("Location: ../auth/login.php"); 
        exit();
    }
    ?>
</head>

<body>
<?php
require_once('../constants.php');

if(isset($_POST['submit'])){
    $showTheForm = false;
    // get fields
    $name = $_POST['name'];
    $diameter = $_POST['diameter'];
    $mass = $_POST['mass'];
    $mass_exp = $_POST['mass_exp'];
    $gravity = $_POST['gravity'];
    $picture_name = $_FILES['picture']['name'];

    if(!empty($name) && !empty($diameter) && !empty($mass) && !empty($gravity) && !empty($picture_name)){
        //get pic info
        $pictureError = $_FILES['picture']['error'];
        $pictureType = $_FILES['picture']['type'];
        $pictureSize = $_FILES['picture']['size'];
        if($pictureError == 0 && $pictureSize > 0 && ($pictureType == 'image/jpeg' || $pictureType == 'image/png')){
            $tempFolder = $_FILES['picture']['tmp_name'];
            $picture_name = $_FILES['picture']['name'];
            $target = '../'.IMG_PATH.$picture_name;

            $_mass = $mass.'e'.$mass_exp;

            if(move_uploaded_file($tempFolder, $target))
            {
                $dbc = mysqli_connect(HOST, USER, PASSWD, DB) or die('Connection error...');
                $query = "INSERT INTO planets VALUES(0, '$name', '$diameter', '$_mass', '$gravity', '$picture_name', 0);";
                // echo $query;
                mysqli_query($dbc, $query) or die('Error during the execution of SQL query...');	
                mysqli_close($dbc);
                echo '<h2>Success!</h2>';
            }else{ echo '<h2>Error while uploading picture!</h2>'; $showTheForm = true; }
        }
        else{ echo '<h2>Error: only jpg and png are acceptable!</h2>'; $showTheForm = true; }
    }
    else{ echo '<h2>Please fill all fields!</h2>'; $showTheForm = true; }
}else{ $showTheForm = true; }

if($showTheForm)
{
?>
    <h1>Add planet</h1>

    <form id="formId" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
        <table width="1%">
            <tr>
                <td class="label">Name:</td>
                <td><input type="text" id="name" name="name" value="<?php if(!empty($name)){echo $name;} ?>"></td>
            </tr>
            <tr>
                <td class="label">Diameter:</td>
                <td><input type="text" id="diameter" name="diameter" value="<?php if(!empty($diameter)){echo $diameter;} ?>"></td>
            </tr>
            <tr>
                <td class="label">Mass:</td>
                <td><input type="text" id="mass" name="mass" value="<?php if(!empty($mass)){echo $mass;} ?>"></td>
                <td class="label">E</td>
                <td><input type="text" id="mass_exp" name="mass_exp" value="<?php if(!empty($mass_exp)){echo $mass_exp;} ?>"></td>
            </tr>
            <tr>
                <td class="label">Gravity:</td>
                <td><input type="text" id="gravity" name="gravity" value="<?php if(!empty($gravity)){echo $gravity;} ?>"></td>
            </tr>
            <tr>
                <td class="label">Picture:</td>
                <td><input type="file" name="picture"></td>
            </tr>
        </table>
        <div><input class="button" type="submit" name="submit" value="Add" id="add"></div>
    </form>

    <?php
    }
    
    // echo '<div class="button"><a href="../main.html">Back</a></div>';
    echo '<div class="container"><a class="button" href="../main.html">Back</a></div>'
    ?>
</body>

</html>