<!DOCTYPE html>
<html lang="hu-HU">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="editPlanets.css" />
    <script type="text/javascript" src="editPlanets.css"></script>
    <!-- Authentication -->
    <?php
    session_start();
    if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] == false){
        header("Location: ../auth/login.php"); 
        exit();
    }
    ?>
</head>

<body>
    <h1>Planet editor</h1>
    <form id="formId" method="post" action="handleSubmit.php" enctype="multipart/form-data">
    <?php
	require_once('../constants.php');
	$dbc = mysqli_connect(HOST, USER, PASSWD, DB) or die('Connection error...');
	$query = "SELECT * FROM planets p WHERE p.deleted = 0";
	$result = mysqli_query($dbc, $query) or die('Error...');
    
	mysqli_close($dbc);

    while($row = mysqli_fetch_array($result))
    {
        $id = $row['id'];
        $name = $row['name'];
        $diameter = $row['diameter'];
        $mass = $row['mass'];
        $gravity = $row['gravity'];
        $picture = $row['picture'];
        echo "<div id=\"".$id."\">";
        echo "<table>";
        echo "<tbody>";
        echo "    <tr height=\"50px\">";
        echo "        <td></td>";
        echo "        <td></td>";
        echo "        <td rowspan=\"6\"><img src=\"../".IMG_PATH . $picture."\" width=\"400px\"></td>";
        echo "    </tr>";
        echo "    <tr>";
        echo "        <td class=\"label\">Name: </td>";
        echo "        <td><input type=\"text\" name=\"name_".$id."\" value=\"".$name."\"></td>";
        echo "    </tr>";
        echo "    <tr>";
        echo "        <td class=\"label\">Diameter(Km): </td>";
        echo "        <td class=\"label\"><input type=\"text\" name=\"diameter_".$id."\" value=\"".$diameter."\"></td>";
        echo "    </tr>";
        echo "    <tr>";
        echo "        <td class=\"label\">Mass(Kg): </td>";
        echo "        <td class=\"label\"><input type=\"text\" name=\"mass_".$id."\" value=\"".$mass."\"></td>";
        echo "    </tr>";
        echo "    <tr>";
        echo "        <td class=\"label\">Gravity(m/s<sup>2</sup>): </td>";
        echo "        <td class=\"label\"><input type=\"text\" name=\"gravity_".$id."\" value=\"".$gravity."\"></td>";
        echo "    </tr>";
        echo "    <tr>";
        echo "        <td><input class=\"button\" type=\"submit\" name=\"set_".$id."\" value=\"Set\"></td>";
        echo "        <td><input class=\"button\" type=\"submit\" name=\"delete_".$id."\" value=\"Delete\"></td>";
        echo "    </tr>";
        echo "    <tr height=\"50px\">";
        echo "    </tr>";
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    }
    ?>
    </form>
    <div class="container"><a class="centerBtn button" href="../main.html">Back</a></div>
</body>

</html>