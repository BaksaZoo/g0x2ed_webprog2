<!DOCTYPE html>
<html lang="hu-HU">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="editPlanets.css" />
</head>

<body>
    <h1>Planet set</h1>

<?php 
require_once('../constants.php');
$dbc = mysqli_connect(HOST, USER, PASSWD, DB) or die('Connection error...');
$query = "SELECT * FROM planets p WHERE p.deleted = 0";
$result = mysqli_query($dbc, $query) or die('Error...');

while($row = mysqli_fetch_array($result))
    {
        $id = $row['id'];
        $name = $row['name'];
        $diameter = $row['diameter'];
        $mass = $row['mass'];
        $gravity = $row['gravity'];
        $picture = $row['picture'];

        if(isset($_POST['set_'.$id])){
            echo "<p>";
            echo "Name ".$_POST['name_'.$id];
            echo "</p>";
            
            echo "<p>";
            echo "\nDiameter ".$_POST['diameter_'.$id];
            echo "</p>";
            
            echo "<p>";
            echo "\nMass ".$_POST['mass_'.$id];
            echo "</p>";
            
            echo "<p>";
            echo "\nGravity ".$_POST['gravity_'.$id];
            echo "</p>";
            
            echo "<p>";
            echo "\nPicture ".$picture;
            echo "</p>";

            $query = "UPDATE planets p SET
                name = \"".$_POST['name_'.$id]."\"
                , diameter = ".$_POST['diameter_'.$id]."
                , mass = ".$_POST['mass_'.$id]."
                , gravity = ".$_POST['gravity_'.$id]."
                WHERE p.id = ".$id.";";
            $result = mysqli_query($dbc, $query) or die('Error...');
            mysqli_close($dbc);

            break;
        }
        if(isset($_POST['delete_'.$id])){
            $query = "UPDATE planets p SET
                deleted = 1
                WHERE p.id = ".$id.";";
            $result = mysqli_query($dbc, $query) or die('Error...');
            mysqli_close($dbc);
            break;
        }
    }
?>

<div class="container"><a class="button" href="editPlanets.php">Back</a></div>

</body>
</html>