<!DOCTYPE html>
<html lang="hu-HU">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="playground.css" />
    <script type="text/javascript" src="playground.js"></script>
</head>

<body>
    <div id="wrapper">

        <div id="canvas"></div>

        <div id="toolbar">
            <div id="buttons">
                <table>
                    <tr height="50px">
                        <td><a class="centerBtn button" href="../main.html">Back</a></td>
                        <td><input type="button" class="centerBtn button" id="btnDistance" onclick="setDistanceMeasure()" value="Distance"/></td>
                    </tr>
                    <tr height="50px"><td><input type="button" class="centerBtn button" onclick="clearDistanceMeasure()" value="Clear"/></td></tr>
                </table>
            </div>
            <div id="container">
            </div>
        </div>
        
        <?php
        require_once('../constants.php');
        $dbc = mysqli_connect(HOST, USER, PASSWD, DB) or die('Connection error...');
        $query = "SELECT * FROM planets p WHERE p.deleted = 0";
        $result = mysqli_query($dbc, $query) or die('Error...');

        while($row = mysqli_fetch_array($result))
        {
            $id = $row['id'];
            $name = $row['name'];
            $diameter = $row['diameter'];
            $mass = $row['mass'];
            $gravity = $row['gravity'];
            $picture = $row['picture'];
            echo '<script type="text/javascript">',
                    'initPlanet('.$id.', "'.$name.'", '.$diameter.', '.$mass.', '.$gravity.', "../'.IMG_PATH.$picture.'");',
                '</script>';
        }
        mysqli_close($dbc);
        ?>

        <div id="linesSvg">
            <svg height="100%" width="100%" id="lineContainer">
                <line id="line" x1="0" y1="0" x2="0" y2="0" stroke="white" z-index="10001"/>
            </svg>
        </div>
    </div>
</body>

</html>