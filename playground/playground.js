var movePlanet;
var mousePosition = { 'x': 0, 'y': 0 };
var planets = [];
var infoDiv;
var containerClick = false;
var hoveredPlanet;
var KM_PER_PIXEL = 100;
var distanceMeasure = false;
var planetsSelected = [];
var distanceDiv;

function getById(id) {
    return document.getElementById(id);
}

function clearDistanceMeasure() {
    distanceMeasure = false;
    var btn = getById("btnDistance");
    btn.style.backgroundColor = "#ffffff1e";
    distanceDiv.style.top = -1000 + "px";
    planetsSelected = [];
    var line = getById("line");
    line.setAttribute("x1", 0 + "px");
    line.setAttribute("y1", 0 + "px");
    line.setAttribute("x2", 0 + "px");
    line.setAttribute("y2", 0 + "px");
}

function setDistanceMeasure() {
    distanceMeasure = !distanceMeasure;
    var btn = getById("btnDistance");
    if (distanceMeasure) {
        btn.style.backgroundColor = "rgba(0, 255, 157, 0.473)";
    } else {
        btn.style.backgroundColor = "#ffffff1e";
    }
}

function canvas_mouseMoveHandler(source) {
    var x = source.clientX;
    var y = source.clientY;
    mousePosition = { 'x': x, 'y': y };

    if (source.target.id.startsWith("planet_"))
        hoveredPlanet = getPlanetById(source.target.id);
    else hoveredPlanet = null;
}

function getPlanetData(planet) {
    return ("Name: " + planet.name + "<br>" +
        "Diameter: " + planet.diameter + " Km<br>" +
        "Mass: " + planet.mass + " Kg<br>" +
        "Gravity: " + planet.gravity + " m/s<sup>2</sup>");
}

function wrapper_onClickHandler(source) {
    if (source.target.id.startsWith("planet_") && source.target.id != movePlanet && !distanceMeasure) {
        movePlanet = source.target.id;

        var img = getById(movePlanet);
        var planet = getPlanetById(movePlanet);
        img.style.width = planet.diameter / 100 + "px";

        img.style.left = mousePosition['x'] - img.width / 2 + "px";
        img.style.top = mousePosition['y'] - img.height / 2 + "px";

        img.style.position = "absolute";

        getById("wrapper").appendChild(img);

        infoDiv.innerHTML = source.target;
    } else if (distanceMeasure && source.target.id.startsWith("planet_")) {
        var planet = getPlanetById(source.target.id);
        var img = getById(source.target.id);

        if (planetsSelected.length < 2) {
            planetsSelected.push(planet);
        } else {
            planetsSelected = [];
            planetsSelected.push(planet);
        }
    } else {
        movePlanet = null;
    }
}

function getPlanetById(id) {
    var actualId = id.replace("planet_", "");
    for (var i = 0; i < planets.length; i++)
        if (planets[i].id == actualId) return planets[i];
    return null;
}

function Planet(id, name, diameter, mass, gravity, picture) {
    this.id = id;
    this.name = name;
    this.diameter = diameter;
    this.mass = mass;
    this.gravity = gravity;
    this.picture = picture;
}

function initPlanet(id, name, diameter, mass, gravity, picture) {
    var planet = new Planet(id, name, diameter, mass, gravity, picture);
    planets.push(planet);

    var img = document.createElement("img");
    img.src = picture;
    img.id = "planet_" + id;
    img.style.width = "100px";
    img.style.position = "relative";
    getById("container").appendChild(img);
}

function loop() {
    infoDiv.style.left = mousePosition["x"] + 20 + "px";
    infoDiv.style.top = mousePosition["y"] + 20 + "px";

    if (distanceMeasure) {
        infoDiv.innerHTML = "Select planets!";
    } else if (hoveredPlanet != null) {
        infoDiv.innerHTML = getPlanetData(hoveredPlanet);
    } else {
        infoDiv.innerHTML = "Click on a planet!";
    }
    if (movePlanet != null) {
        var img = getById(movePlanet);

        var planet = getPlanetById(movePlanet);
        infoDiv.style.left = mousePosition["x"] + planet.diameter / 2 / 100 + 10 + "px";
        infoDiv.style.top = mousePosition["y"] + "px";

        img.style.left = mousePosition['x'] - img.width / 2 + "px";
        img.style.top = mousePosition['y'] - img.height / 2 + "px";

        infoDiv.innerHTML = getPlanetData(getPlanetById(movePlanet));
    }

    line = getById("line");
    if (planetsSelected.length == 1) {
        var x1 = getById("planet_" + planetsSelected[0].id).offsetLeft + planetsSelected[0].diameter / 200;
        var y1 = getById("planet_" + planetsSelected[0].id).offsetTop + planetsSelected[0].diameter / 200;
        var x2 = mousePosition["x"];
        var y2 = mousePosition["y"];
        line.setAttribute("x1", x1 + "px");
        line.setAttribute("y1", y1 + "px");
        line.setAttribute("x2", x2 + "px");
        line.setAttribute("y2", y2 + "px");

        length = Math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2);
        distanceDiv.innerHTML = (length * KM_PER_PIXEL).toFixed(3) + " Km";

        distanceDiv.style.left = x1 + (x2 - x1) / 2 + 10 + "px";
        distanceDiv.style.top = y1 + (y2 - y1) / 2 + 10 + "px";
    } else if (planetsSelected.length == 2) {
        var x1 = getById("planet_" + planetsSelected[0].id).offsetLeft + planetsSelected[0].diameter / 200;
        var y1 = getById("planet_" + planetsSelected[0].id).offsetTop + planetsSelected[0].diameter / 200;
        var x2 = getById("planet_" + planetsSelected[1].id).offsetLeft + planetsSelected[1].diameter / 200;
        var y2 = getById("planet_" + planetsSelected[1].id).offsetTop + planetsSelected[1].diameter / 200;
        line.setAttribute("x1", x1 + "px");
        line.setAttribute("y1", y1 + "px");
        line.setAttribute("x2", x2 + "px");
        line.setAttribute("y2", y2 + "px");

        length = Math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2);
        distanceDiv.innerHTML = (length * KM_PER_PIXEL).toFixed(3) + " Km";

        distanceDiv.style.left = x1 + (x2 - x1) / 2 + 10 + "px";
        distanceDiv.style.top = y1 + (y2 - y1) / 2 + 10 + "px";
    }
}

function start() {
    getById('wrapper').addEventListener('mousemove', canvas_mouseMoveHandler, false);
    getById('wrapper').addEventListener('click', wrapper_onClickHandler, false);

    window.setInterval(loop, 20);

    // setup infoDiv
    infoDiv = document.createElement("div");
    infoDiv.id = "infoDiv";
    infoDiv.innerHTML = "Click on a planet!";
    infoDiv.addEventListener('mousemove', canvas_mouseMoveHandler, false);
    document.body.appendChild(infoDiv);

    // setup distanceDiv
    distanceDiv = document.createElement("div");
    distanceDiv.id = "distanceDiv";
    distanceDiv.innerHTML = "distance";
    distanceDiv.addEventListener('mousemove', canvas_mouseMoveHandler, false);
    distanceDiv.style.top = -1000 + "px";
    document.body.appendChild(distanceDiv);
}


window.addEventListener('load', start, false);