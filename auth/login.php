<?php 
if(!session_id())
{
    session_start();
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv = "Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="login.css"/>
</head>
<body>
    <h1>Log in</h1>
    <?php
    require_once('../constants.php');
    if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true){
        echo '<h2>You\'re already logged in.</h2>';
        echo '<div class="container"><a class="centerBtn button" href="../main.html">Back</a></div>';
    }
    else{
    ?>

    <form method="post" action="auth.php">
        <table class="center">
            <p hidden> <input type="text" name="url" value="../main.html"> </p>
            <tr>
                <td class="label">Username: </td>
                <td><input type="text" name="username" value="<?php if(!empty($username)){echo $username;} ?>"/></td>
            </tr>
            <tr>
                <td class="label">Password: </td>
                <td><input type="password" name="password"/></td>
            </tr>
        </table>
        
        <div class="container"><input class="centerBtn button" type="submit" name="submit" value="Log in"/></div>
    </form>
    <div class="container"><a class="centerBtn button" href="../main.html">Back</a></div>
    
    <?php } ?>
</body>
</html>









